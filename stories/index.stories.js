import koatuu from '../public/koatuu.html';
import textArea from '../public/textArea.html';

export default {
  title: 'Controls',
};

export const TextArea = () => textArea;

export const Koatuu  = () => koatuu;

